## redfin-user 12 SPB5.210812.002 7671067 release-keys
- Manufacturer: xiaomi
- Platform: sdm660
- Codename: whyred
- Brand: Xiaomi
- Flavor: fluid_whyred-userdebug
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: eng.arnavp.20211021.165911
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/redfin/redfin:12/SPB5.210812.002/7671067:user/release-keys
- OTA version: 
- Branch: redfin-user-12-SPB5.210812.002-7671067-release-keys
- Repo: xiaomi_whyred_dump_5315


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
